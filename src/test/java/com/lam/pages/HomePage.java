package com.lam.pages;

import java.awt.event.KeyEvent;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

import com.lam.basePage.BasePageClass;
import com.lam.baseTest.BaseTestClass;
import com.lam.util.Log;
import com.lam.util.SikuliThread;

public class HomePage extends BasePageClass{

	private final By VCC_MANAGEMENT_LOC = By.xpath("//div[@class='desc' and contains(text(),'Administration')]");
	 private final By VCC_COLLABORATION_LOC = By.xpath("//div[@class='desc' and contains(text(),'Collaboration')]");
	 private final By VCC_EXECUTIVE_LOC = By.xpath("//div[@class='desc' and contains(text(),'Reports & Dashboards')]");
	 private final By VCC_MAIL_LOC = By.xpath("//div[@class='desc' and contains(text(),'Email')]");

	public HomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public boolean verifyVCCManagementTab(){
		try{
			driver.findElement(VCC_MANAGEMENT_LOC);
			return true;
		}
		catch(NoSuchElementException E){
			return false;
		}

	}

	public boolean verifyVCCMailTab(){
		try{
			driver.findElement(VCC_MAIL_LOC);
			return true;
		}
		catch(NoSuchElementException E){
			return false;
		}

	}

	public boolean verifyVCCCollaborationTab() throws InterruptedException{
		try{
			waitForElementPresent(VCC_COLLABORATION_LOC);
			waitForElementVisible(VCC_COLLABORATION_LOC);
			driver.findElement(VCC_COLLABORATION_LOC);
			return true;
		}
		catch(NoSuchElementException E){
			return false;
		}

	}

	public boolean verifyVCCExecutiveTab() throws InterruptedException{
		try{
			waitForElementPresent(VCC_EXECUTIVE_LOC);
			waitForElementVisible(VCC_EXECUTIVE_LOC);
			driver.findElement(VCC_EXECUTIVE_LOC);
			return true;
		}
		catch(NoSuchElementException E){
			return false;
		}

	}

	public VCCCollaborationPage clickOnVCCCollaborationTab() throws InterruptedException {
		Log.info("waiting for VCC Collaboration Tab");
		waitForElementPresent(VCC_COLLABORATION_LOC);
		driver.findElement(VCC_COLLABORATION_LOC).click();	
		Log.info("VCC Collaboration Tab is clicked");
		return new VCCCollaborationPage(driver);
	}

	public VCCManagementPage clickOnVCCManagementTab() throws InterruptedException {
		Log.info("Waiting for VCC Management Tab");
		Thread.sleep(5000);
        driver.navigate().refresh();
        if(isAlertPresent()==true){
        	Alert alert = driver.switchTo().alert();
        	alert.accept();
        }
		waitForElementPresent(VCC_MANAGEMENT_LOC);
		driver.findElement(VCC_MANAGEMENT_LOC).click();
		Log.info("VCC Management Tab clicked");
		return new VCCManagementPage(driver);
	}


	public VCCExecutivePage clickOnVCCExecutiveTab(String username,String password) throws InterruptedException {

		if(BaseTestClass.getBrowser().equalsIgnoreCase("chrome")) {
			SikuliThread httpThread = new SikuliThread(username, password);
			Thread authticationThread = new Thread(httpThread);
			authticationThread.start();
			Log.info("Waiting for VCC Executive Tab");
			waitForElementPresent(VCC_EXECUTIVE_LOC);
			driver.findElement(VCC_EXECUTIVE_LOC).click();
			Log.info("VCC executive Tab clicked");
			while(true){
				if(!authticationThread.isAlive())
					break;
			}
		}
		else{
			driver.findElement(VCC_EXECUTIVE_LOC).click();
			Log.info("VCC executive Tab clicked");
			Thread.sleep(15000);
			handleHTTPAuthenticationLogin(username, password);
		}
//		try {
//			driver.findElement(VCC_EXECUTIVE_LOC).click();
//			Thread.sleep(15000);
//		} catch(TimeoutException timeout) {			
//			((JavascriptExecutor)driver).executeScript("window.stop()");
//			robot.keyPress(KeyEvent.VK_F5);	
//			Thread.sleep(10000);
//		}			
//		handleHTTPAuthenticationLogin(username, password);			
		Thread.sleep(5000);
		return new VCCExecutivePage(driver);
	}




}





