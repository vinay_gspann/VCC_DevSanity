package com.lam.pages;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

import com.lam.basePage.BasePageClass;
import com.lam.baseTest.BaseTestClass;
import com.lam.util.SikuliThread;

public class LoginPage extends BasePageClass {

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	@SuppressWarnings("deprecation")
	public HomePage loginToApplication(String URL,String username,String password) throws InterruptedException, IOException{
		if(BaseTestClass.getBrowser().equalsIgnoreCase("chrome")) {
			SikuliThread httpThread = new SikuliThread(username, password);
			Thread authticationThread = new Thread(httpThread);
			authticationThread.start();			
			driver.get(URL);
			while(true){
				if(!authticationThread.isAlive())
					break;
			}
		}
			else{
				try{			
					((JavascriptExecutor)driver).executeScript("window.focus()");
					driver.get(URL);			
					if(BaseTestClass.getBrowser().equalsIgnoreCase("ie")){
						Thread.sleep(7000);
						handleHTTPAuthenticationLogin(username, password);						
						Thread.sleep(10000);
					}
				}
				catch(TimeoutException e){			
					handleHTTPAuthenticationLogin(username, password);					
				}
			}
		Log.info("Username and password entered");
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		return new HomePage(driver);
	}

}
