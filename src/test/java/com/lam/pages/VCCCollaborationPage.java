package com.lam.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.lam.basePage.BasePageClass;
import com.lam.baseTest.BaseTestClass;
import com.lam.util.Log;
import com.lam.util.SikuliThread;

public class VCCCollaborationPage extends BasePageClass{
	final String location="src\\test\\resources\\data\\uploadNewFiles\\";
	final By NEW_DOCUMENT_LOC = By.xpath("//span[text()='new document']");
	final By DLG_FRAME_LOC = By.cssSelector("iframe[class='ms-dlgFrame']");
	final By BROWSE_BTN_LOC = By.xpath("//input[@title='Choose a file']");
	final By OK_BTN_LOC =By.xpath("//input[@value='OK']");
	final By MANAGE_ACCESS_START_BTN_LOC = By.xpath("//button[@id='start']");
	final By USERS_AND_GROUPS_AUTO_SUGGESTION_LOC = By.xpath("//div[@class='ms-core-menu-label']"); 
	final By USERS_AND_GROUPS_LOC = By.xpath("//input[@title='User or Group']");
	final By DELETE_ITEM_FROM_MENU_LOC = By.xpath("//div[@id='ID_Delete Item']");
	final By MANAGE_ACCESS_FROM_MENU_LOC = By.xpath("//div[@id='ID_Manage Access']");
	final By VIEW_IN_SECURE_COLLABORATION_LOC = By.xpath("//div[@id='ID_View in Secure Collaboration']");
	final By DELETE_FILE_BTN_LOC = By.xpath("//button[@id='start'][contains(text(),'Delete')]");
	final By DELETE_FILE_CONFIRMATION_CHKBOX_LOC = By.xpath("//input[starts-with(@id,'DeteteConfirmation')]");
	final By DELETE_POPUP_CLOSE_BTN_LOC = By.xpath("//div[@class='ms-dlgFrameContainer']//h2//ancestor::div[@class='ms-dlgContent']//a[contains(@id,'DlgClose')]/span/span");
	final By PRINT_ICON_SECURECOLLABORATION_LOC=By.xpath("//div[@id='toolbarContainer']/div/button[contains(@title,'Print')]");
	final By SHARED_DOCUMENT_ICON_LOC = By.xpath("//div[@id='s4-bodyContainer']/div/div/div/ul/li/a/span[contains(text(),'View Shared Documents')]//preceding::i[1]");
	final By OPEN_MENU_CLOSE_BTN_LOC = By.xpath("//img[@title='Close']");
	final By PRINT_BTN_LOC = By.xpath("//button[@title='Print']");
	final By DELETE_CONFIRMATION_MSG_LOC = By.xpath("//span[@class='text-danger']");
	final By SECURE_COLLAB_FILE_IMG_LOC = By.xpath("//img[@id='mainImg']");
	final By RIGHT_CLICK_DISABLED_LOC = By.xpath("//span[text()='Sorry! Right click is disabled in this page!']");
	final By ADD_A_DOCUMENT_LOC = By.xpath("//h1[@id='dialogTitleSpan']");
	final By SHARED_DOCUMENT_ACTIVATION_LOC = By.xpath("//div[@id='s4-bodyContainer']/div/div/div/ul/li/a/span[contains(text(),'View Shared Documents')]//ancestor::li[@class='primary-nav active open']");
	final By DELETE_WARNING_MSG_LOC = By.xpath("//i[@class='fa fa-exclamation-triangle text-danger']");
	
	
	String encryptedFileNameLoc="//a[contains(text(),'%s')]//ancestor::td[contains(@class,'title')]//following::td[1]/div[not(@id)]//h2";
	String documentFileCheckboxLoc = "//div[@data-name='WebPartZone']//div[contains(@id,'MSOZoneCell')][1]//a[text()='%s']/preceding::td[2]";
	String openMenuDotsLoc = "//div[@data-name='WebPartZone']//div[contains(@id,'MSOZoneCell')][1]//a[text()='%s']/following::a[@title='Open Menu'][1]";
	String subMenuDotsLoc = "//h2[text()='%s.nxl']/following::span[@class='js-callout-ecbMenu']/a";
	String myDocumentsFirstRowLoc = "//script[@id='scriptBodyWPQ2']/following::tr[1]//a[contains(@class,'ms-listlink') and text()='%s']"; 
	String myDocumentsFileLoc = "//a[contains(@class,'ms-listlink') and text()='%s']";

	public VCCCollaborationPage(WebDriver driver) {
		super(driver);		 
	}

	public boolean uploadNewDocumentAndVerifyInRows(String fileName) throws InterruptedException, AWTException{
		Log.info("Waiting for New Document button");
		waitForElementPresent(NEW_DOCUMENT_LOC);
		Thread.sleep(5000);
		driver.findElement(NEW_DOCUMENT_LOC).click();
		Log.info("New Document button clicked..waiting for frame now");
		waitForElementPresent(DLG_FRAME_LOC);
		driver.switchTo().frame(driver.findElement(DLG_FRAME_LOC));
		Log.info("frame switched..waiting for browse button");
		waitForElementVisible(BROWSE_BTN_LOC);
		if(BaseTestClass.getBrowser().equalsIgnoreCase("ie"))		{
			Actions action = new Actions(driver);
			action.moveToElement(driver.findElement(BROWSE_BTN_LOC)).doubleClick().build().perform();
			Log.info("Browse button clicked");
		}
		else {
			driver.findElement(BROWSE_BTN_LOC).click();
		}
		Log.info("Browse button clicked");
		Thread.sleep(2000);
		Robot robot=new Robot();
		File f = new File(location+fileName);			
		setClipboardData(f.getAbsolutePath());
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.delay(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(1000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		driver.findElement(OK_BTN_LOC).click();
		Log.info("OK button clicked");
		Thread.sleep(2000);
		for(int i=1;i<=5;i++){
			Log.info("entering loop for browse button");
			driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			try{
				if (driver.findElements(By.xpath("//span[@role='alert' and contains(text(),'You must specify a value for the required field.')]")).size()!=0){
					Log.info("Browse file not worked fine in this attempt");
					robot.keyPress(KeyEvent.VK_RIGHT);
					robot.delay(1000);
					robot.keyRelease(KeyEvent.VK_RIGHT);
					robot.delay(1000);
					robot.keyPress(KeyEvent.VK_ENTER);
					robot.delay(1000);
					robot.keyRelease(KeyEvent.VK_ENTER);
					Thread.sleep(2000);
					driver.findElement(OK_BTN_LOC).click();
					Thread.sleep(3000);
				}
				else{
					Log.info("Browse button has worked");
					break;
				}

			}catch(Exception e){
				Log.info("Browse file worked fine in first attempt");
				break;
			}	
		}				

		driver.switchTo().defaultContent();
		Log.info("switch to default content and upload window is closed");
		Log.info("Waiting for file to appear in documents");
		waitForElementNotVisible(ADD_A_DOCUMENT_LOC);
		waitForFileToAppearInDocumentAfterUpload(fileName);
        // This is additional delay introduced,because delete process was not working fine,due to workflow delays
        Thread.sleep(10000);
		return isTheFilePresentInRows(fileName);
	}

	public VCCCollaborationPage clickOnOpenMenuDots(String fileName) throws InterruptedException{		
		driver.findElement(By.xpath(String.format(documentFileCheckboxLoc, fileName))).click();
		Thread.sleep(2000);
		Log.info("Check box clicked for the file "+fileName);
		driver.findElement(By.xpath(String.format(openMenuDotsLoc, fileName))).click();	
		if(!isElementPresent(OPEN_MENU_CLOSE_BTN_LOC)){
			driver.findElement(By.xpath(String.format(documentFileCheckboxLoc, fileName))).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath(String.format(documentFileCheckboxLoc, fileName))).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath(String.format(openMenuDotsLoc, fileName))).click();	
		}
		if(BaseTestClass.getBrowser().equals("firefox")){
			Thread.sleep(2000);
			driver.findElement(By.xpath(String.format(openMenuDotsLoc, fileName))).click();
		}		
		Log.info("Open Menu Dots clicked");
		return new VCCCollaborationPage(driver);
	}

	public boolean isFileInEncryptedForm(String fileName){
		String fullNameOfFile=driver.findElement(By.xpath(String.format(encryptedFileNameLoc,fileName))).getText();
		if(fullNameOfFile.contains(".nxl")){
			Log.info("File is encrypted");
			return true;
		}
		return false;
	}

	public VCCCollaborationPage clickOnOpenSubMenuDots(String fileName) throws InterruptedException{		
		if(BaseTestClass.getBrowser().equalsIgnoreCase("firefox")){
			for(int i=1;i<=5;i++){
				if ((isElementPresent(By.xpath("//span[text()='Shared with ']"))==false) || 
						(isElementPresent(By.xpath(String.format("//h2[text()='%s.nxl']/ancestor::div[@class='js-callout-content']//td[@class='ms-paging']", fileName))))==true){
					Log.info("Menu pop up not loaded,click on popup menu close button");
					driver.findElement(OPEN_MENU_CLOSE_BTN_LOC).click();
					Thread.sleep(3000);
					Log.info("popup menu closed");
					driver.findElement(By.xpath(String.format(openMenuDotsLoc, fileName))).click();
					Thread.sleep(2000);
					Log.info("Re-open popup menu");
				}
				else{
					Log.info("Menu popup loaded ");
					break;
				}

			}

		}
		Log.info("waiting for submenu dots");
		waitForElementPresent(By.xpath(String.format(subMenuDotsLoc, fileName)));
		driver.findElement(By.xpath(String.format(subMenuDotsLoc, fileName))).click();
		Log.info("Sub Menu Dots clicked");
		Thread.sleep(3000);
		return new VCCCollaborationPage(driver);
	}	

	public VCCCollaborationPage selectFromMenuItems(String menuItem) throws InterruptedException, IOException{		
		if(menuItem.equalsIgnoreCase("Delete Item")){		
			Log.info("waiting for delete item from menu");
			waitForElementPresent(DELETE_ITEM_FROM_MENU_LOC);
			driver.findElement(DELETE_ITEM_FROM_MENU_LOC).click();
		}
		if(menuItem.equalsIgnoreCase("Manage Access")){
			Log.info("waiting for manage access from menu");
			waitForElementPresent(MANAGE_ACCESS_FROM_MENU_LOC);
			driver.findElement(MANAGE_ACCESS_FROM_MENU_LOC).click();
		}		
		Log.info(menuItem+" clicked");
		Thread.sleep(10000);		
		return new VCCCollaborationPage(driver);
	}

	public VCCCollaborationPage selectAndLoginToViewInSecureCollaboration(String username,String password) throws InterruptedException{
		if(BaseTestClass.getBrowser().equalsIgnoreCase("chrome")) {
			SikuliThread httpThread = new SikuliThread(username, password);
			Thread authticationThread = new Thread(httpThread);
			authticationThread.start();
			Log.info("waiting for View In Secure COllaboration link");
			waitForElementPresent(VIEW_IN_SECURE_COLLABORATION_LOC);
			driver.findElement(VIEW_IN_SECURE_COLLABORATION_LOC).click();
			while(true){
				if(!authticationThread.isAlive())
					break;
			}
		}
		else{
			Log.info("waiting for View In Secure COllaboration link");
			waitForElementPresent(VIEW_IN_SECURE_COLLABORATION_LOC);
			driver.findElement(VIEW_IN_SECURE_COLLABORATION_LOC).click();			
			Thread.sleep(10000);
			handleHTTPAuthenticationLogin(username, password);
		}
		Log.info("Authentication done");
		Thread.sleep(20000);
		return new VCCCollaborationPage(driver);
	}

	public String getDeleteConfirmationMsg() throws InterruptedException{
		Thread.sleep(8000);
		driver.switchTo().frame(driver.findElement(DLG_FRAME_LOC));
		Log.info("Frame switched");
		waitForElementDisplayed(DELETE_WARNING_MSG_LOC);
		waitForElementPresent(DELETE_CONFIRMATION_MSG_LOC);
		//waitForElementVisible(DELETE_CONFIRMATION_MSG_LOC);
		Log.info(driver.findElement(DELETE_CONFIRMATION_MSG_LOC).getText());
		return driver.findElement(DELETE_CONFIRMATION_MSG_LOC).getText();
	}

	public VCCCollaborationPage selectConfirmAndDeleteSelectedFile(String fileName) throws InterruptedException, AWTException{
		Thread.sleep(5000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame(driver.findElement(DLG_FRAME_LOC));
		driver.findElement(DELETE_FILE_CONFIRMATION_CHKBOX_LOC).click();
		Thread.sleep(5000);
		driver.findElement(DELETE_FILE_BTN_LOC).click();
		Thread.sleep(5000);
		return new VCCCollaborationPage(driver);		
	}

	public VCCCollaborationPage clickOnCloseAfterDeleteMsgPopup(String fileName) throws InterruptedException{
		driver.findElement(DELETE_POPUP_CLOSE_BTN_LOC).click();
		Thread.sleep(2000);
		if (BaseTestClass.getBrowser().equalsIgnoreCase("ie")){	        
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			pageRefreshingAfterDeletion(fileName);
		}
		else if (BaseTestClass.getBrowser().equalsIgnoreCase("chrome")) {
			pageRefreshingAfterDeletion(fileName);
		}
		else if (BaseTestClass.getBrowser().equalsIgnoreCase("firefox")){	        
			Thread.sleep(2000);
			if(isAlertPresent()==true){	
				Log.info("Alert found");
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Log.info("Alert handled");
				Thread.sleep(10000);
			}
			for(int i=1;i<=4;i++){
				if(isTheFilePresentInRows(fileName)==true){
					driver.navigate().refresh();
					Thread.sleep(12000);
					if(isAlertPresent()==true){
						Log.info("Alert found");
						Alert alert = driver.switchTo().alert();
						alert.accept();
						Log.info("Alert handled");
						Thread.sleep(10000);
					}
				}
				else
					break;
			}

		}

		return new VCCCollaborationPage(driver);
	}

	public void pageRefreshingAfterDeletion(String fileName) throws InterruptedException {
		for(int i=1;i<=4;i++){
			if(isTheFilePresentInRows(fileName)==true){
				try{
					driver.navigate().refresh();	
					if (BaseTestClass.getBrowser().equalsIgnoreCase("ie")){	        
						robot.keyPress(KeyEvent.VK_ENTER);
						robot.keyRelease(KeyEvent.VK_ENTER);
					}
				}catch (UnhandledAlertException e) {
					Alert alert = driver.switchTo().alert();
					alert.accept();
					Thread.sleep(3000);
				}				
			}
			else
				break;
		}
	}

	public String getDeleteSuccessMsgAppears(){
		driver.switchTo().defaultContent();
		Log.info(driver.findElement(By.xpath("//div[@class='ms-dlgFrameContainer']//h2")).getText());
		return driver.findElement(By.xpath("//div[@class='ms-dlgFrameContainer']//h2")).getText();

	}

	public VCCCollaborationPage manageAccessForUser(String user) throws InterruptedException{
		//add the parameters for String of users, due date,permission,level and access options later
		Log.info("Waiting for Manage access start button to verify the workflow UI has loaded completely");
		waitForElementVisible(MANAGE_ACCESS_START_BTN_LOC);
		driver.findElement(USERS_AND_GROUPS_LOC).sendKeys(user);
		Thread.sleep(4000);
		waitForElementPresent(USERS_AND_GROUPS_AUTO_SUGGESTION_LOC);
		//		driver.findElement(USERS_AND_GROUPS_LOC).sendKeys("\t");
		Thread.sleep(2000);		
		driver.findElement(USERS_AND_GROUPS_AUTO_SUGGESTION_LOC).click();
		Thread.sleep(2000);
		driver.findElement(MANAGE_ACCESS_START_BTN_LOC).click();			
		Thread.sleep(10000);
		waitForElementNotVisible(By.xpath("//span[text()='Please wait...']"));
		return new VCCCollaborationPage(driver);
	}

	public boolean isTheFilePresentAtFirstRow(String fileName) throws InterruptedException{
		try{
			Thread.sleep(10000);
			driver.findElement(By.xpath(String.format(myDocumentsFirstRowLoc, fileName)));
			return true;
		}catch(NoSuchElementException e){
			return false;
		}
	}

	public void waitForFileToAppearInDocumentAfterUpload(String fileName) throws InterruptedException{
		waitForElementPresent(By.xpath("//a[text()='"+fileName+"']/following::span[contains(text(),'A few seconds')][1]"));
	}

	public boolean isTheFilePresentInRows(String fileName) throws InterruptedException{
		//		try{
		//			Thread.sleep(5000);
	    driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		List<WebElement> allDocumentFiles = driver.findElements(By.xpath(String.format(myDocumentsFileLoc, fileName)));
		if (allDocumentFiles.size()!=0){
			Log.info(fileName+" file is present in the rows");
			driver.manage().timeouts().implicitlyWait(120,TimeUnit.SECONDS);
			return true;
		}
		else {
			Log.info(fileName+" file is not present in the rows");
			driver.manage().timeouts().implicitlyWait(120,TimeUnit.SECONDS);
			return false;
		}

		//		}catch(NoSuchElementException e){
		//			Log.info(fileName+" file is not present in the rows");
		//			return false;
		//		}
	}

	public boolean waitAndverifyForFileVisibleInRows(String fileName){
		for(int time=1;time<=5;time++){
			if(driver.findElements(By.xpath(String.format(myDocumentsFileLoc,fileName))).size()==0){
				driver.navigate().refresh();
			}
			else{
				return true;
			}
		}	
		return false;
	}

	public VCCCollaborationPage clickOnSharedDocumentIcon() throws InterruptedException {
		waitForElementPresent(SHARED_DOCUMENT_ICON_LOC);
		driver.findElement(SHARED_DOCUMENT_ICON_LOC).click();
		Log.info("Clicked on Shared Document Icon");
		Thread.sleep(6000);
		waitForElementPresent(SHARED_DOCUMENT_ACTIVATION_LOC);
		return new VCCCollaborationPage(driver);
	}


	public boolean isPrintButtonEnabled() throws InterruptedException{	
		Thread.sleep(5000);
		Log.info("waiting for Print button in secure collaboration view");
		waitForElementPresent(PRINT_BTN_LOC);		
		return driver.findElement(PRINT_BTN_LOC).isEnabled();
	}


	public boolean isRightClickDisabled() throws InterruptedException{
		Actions action = new Actions(driver);	
		Log.info("waiting for background image in secure collaboration view");
		waitForElementPresent((SECURE_COLLAB_FILE_IMG_LOC));
		Thread.sleep(3000);
		action.contextClick(driver.findElement(SECURE_COLLAB_FILE_IMG_LOC)).build().perform();
		Thread.sleep(1000);		
		return isElementPresent(RIGHT_CLICK_DISABLED_LOC);
	}

}