package com.lam.pages;

import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.lam.basePage.BasePageClass;
import com.lam.baseTest.BaseTestClass;

public class Header extends BasePageClass{
	
	private final By dropDownArrowLoc = By.xpath("//a[contains(@id,'Menu')]"); 
	private final By signOutLoc = By.xpath("//div[@id='zz3_ID_Logout']/span[text()='Sign Out']");
	
	public Header(WebDriver driver) {
		super(driver);
	}
	
	public void clickSignOut(){
		driver.findElement(dropDownArrowLoc).click();
		driver.findElement(signOutLoc).click();
	}
	
	public void clickSignOutCompletely() throws InterruptedException{
//		Thread.sleep(5000);
		waitForElementVisible(dropDownArrowLoc);
		driver.findElement(dropDownArrowLoc).click();
		waitForElementVisible(signOutLoc);
		driver.findElement(signOutLoc).click();
		Thread.sleep(2000);
		if(BaseTestClass.getBrowser().equalsIgnoreCase("ie")){
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);			
		}
		driver.close();
	}
		
}
