package com.lam.pages;

import org.eclipse.jetty.util.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.lam.basePage.BasePageClass;


public class LeftnavigationPanel extends BasePageClass{
	
	private final By BACK_TO_VCC_LOC=By.xpath("//span[contains(text(),'Back to My VCC')]//ancestor::a");

	public LeftnavigationPanel(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public HomePage clickBackToVCC(){
		driver.findElement(BACK_TO_VCC_LOC).click();
		Log.info("Clicked on Back to VCC");
		return new HomePage(driver);
	}


}
