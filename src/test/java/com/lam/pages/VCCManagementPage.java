package com.lam.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.lam.basePage.BasePageClass;
import com.lam.util.Log;


public class VCCManagementPage extends BasePageClass {
	final By NEW_DOCUMENT_LOC=By.xpath("//img[contains(@id,'idHomePageNewDocument')]");
	final By CERT_OF_DESTRUCTI0N_LOC = By.xpath("//a[@title='Certificates of Destruction']");
	final By DELETION_TRACKER_LOC = By.xpath("//a[text()='Item Deletion Tracker']");

	String certificationOfDeletionFirstRowLoc = "//a[@class='ms-listlink ms-draggable' and contains(text(),'%s.nxl')]"; 
	String deletionTrackerFirstRowLoc = "//a[@class='ms-listlink' and contains(text(),'%s')]";

	public VCCManagementPage(WebDriver driver) {
		super(driver);
	}


	public VCCManagementPage clickOnCertificationOfDestruction() throws InterruptedException {
		driver.findElement(CERT_OF_DESTRUCTI0N_LOC).click();		
		Log.info("Certification of Destruction clicked");
		Thread.sleep(3000);
		if(isAlertPresent()==true){
			Alert alert = driver.switchTo().alert();
			alert.accept();
		}
		
		return new VCCManagementPage(driver);
	}

	public VCCManagementPage clickOnDeletionTracker() {
		driver.findElement(DELETION_TRACKER_LOC).click();
		Log.info("Deletion Tracker Link clicked");
		return new VCCManagementPage(driver);
	}

	public boolean isTheFilePresentAtFirstRowOfCertificationDestruction(String fileName){
				try{			
					driver.findElement(By.xpath(String.format(certificationOfDeletionFirstRowLoc, fileName)));
					return true;
				}catch(NoSuchElementException e){
					return false;
				}
//		return isElementPresent(By.xpath(String.format(certificationOfDeletionFirstRowLoc, fileName)));
	}	

	public boolean isFileContainsDocxExtension(String fileName){
		boolean isFileContainsDocxExtension = false;
		Log.info("Looking for file to have docx extension");
		String hrefForDocxValidation = driver.findElement(By.xpath(String.format(certificationOfDeletionFirstRowLoc, fileName))).getAttribute("href");
		if(hrefForDocxValidation.endsWith("docx")){
			isFileContainsDocxExtension = true;
		}
		return isFileContainsDocxExtension;
	}

	public boolean isTheFilePresentAtFirstRowOfDeletionTracker(String fileName){
		try{
			waitForElementVisible(By.xpath(String.format(deletionTrackerFirstRowLoc,fileName)));
			driver.findElement(By.xpath(String.format(deletionTrackerFirstRowLoc, fileName)));
			return true;
		}catch(NoSuchElementException e){
			return false;
		}
	}

	public VCCManagementPage clickOnBackPage() throws InterruptedException {
		driver.navigate().back();
		Log.info("Back to previous Page");
		Thread.sleep(2000);
		if(isAlertPresent()==true){
			Alert alert= driver.switchTo().alert();
			alert.accept();
		}
		Thread.sleep(2000);
		if(isAlertPresent()==true){
			Alert alert= driver.switchTo().alert();
			alert.accept();
		}
		Thread.sleep(2000);
		if(isAlertPresent()==true){
			Alert alert= driver.switchTo().alert();
			alert.accept();
		}
		return new VCCManagementPage(driver);
	}

}
