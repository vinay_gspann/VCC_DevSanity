package com.lam.basePage;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.lam.baseTest.BaseTestClass;
import com.lam.util.Log;

public class BasePageClass {
	protected WebDriver driver;
	protected Robot robot;
	protected final String AUTOIT_FILE_LOCATION = "src\\test\\resources\\autoITScript\\IE_Authentication.exe";

	public BasePageClass(WebDriver driver) {
		this.driver =driver;
		try {
			robot =  new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setClipboardData(String string) {
		StringSelection stringSelection = new 	StringSelection(string);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public void clearClipBoardData(){
		StringSelection stringSelection = new 	StringSelection("");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

	public void waitForElementPresent(By locator) throws InterruptedException{
		boolean isFound =false;
		driver.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);
		Log.info("Waiting for the element to interact");
		for (int second = 1;second<=30;) {			
			Log.info("waited for "+second+" seconds");
			try{
				if (isElementPresent(locator)==true){
					Log.info("Wait ended...element found");
					isFound=true;
					break ;
				}
				else{
					Log.info("Element not present yet");
					second++;
					Thread.sleep(1500);
				}

			}catch (Exception e){
				Log.info("No such element found yet");
				second++;
			}
		}
		if(isFound==false)
			Log.info("Wait Timeout...element not present");		
		driver.manage().timeouts().implicitlyWait(120,TimeUnit.SECONDS);
	}

	public void waitForElementVisible(By locator){
		WebDriverWait wait = new WebDriverWait(driver, 120);
		Log.info("Waiting for the element to visible");
		wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		Log.info("Wait ended...element  visible");
	}
	public void waitForElementNotVisible(By locator){
		WebDriverWait wait = new WebDriverWait(driver, 120);
		Log.info("Waiting for the element not visible");
		wait.until(ExpectedConditions.invisibilityOfElementLocated((locator)));
		Log.info("Wait ended...element not visible");
	}

	public boolean isElementPresent(By locator){
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try{
			driver.findElement(locator);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return true;
		}catch(NoSuchElementException e){
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			return false;
		}
	}

	public boolean isAlertPresent(){
		try{
			driver.switchTo().alert();
			return true;
		}catch(Exception e){
			return false;
		}
	}

	public void switchToLastOpenedWindow() throws InterruptedException{	
		Thread.sleep(10000);
		for(int i=1;i<=10;i++){
			ArrayList<String> allWinHandles = new ArrayList<String> (driver.getWindowHandles());
			Log.info("Total number of windows opened are "+allWinHandles.size());
			if(allWinHandles.size()==2){
				driver.switchTo().window(allWinHandles.get(allWinHandles.size()-1));
				Log.info("Switched to last opened window");
				break;
			}
			else{
				Log.info("Vcc Collaboration window has not opened yet");
				Thread.sleep(6000);					
			}
		}		

	}

	public void switchToNewWindow(){
		try{
			ArrayList<String> tabNew = new ArrayList<String> (driver.getWindowHandles());
			// driver.close();
			int intTotalhandles = tabNew.size();
			//Log.info("total handles is :" + intTotalhandles);
			Log.info("Base window URL before switching : "+driver.getCurrentUrl());
			driver.switchTo().window(tabNew.get((intTotalhandles-1)));
			Thread.sleep(3000);
			// Thread.sleep(5000);
			Log.info("New window Title after switching : "+driver.getTitle());
			Log.info("New window URL after switching : "+driver.getCurrentUrl());
		}catch(Throwable t){
			Log.info("Error! Could not switch to the New Window");
		}
	}

	public void switchtoMainwindow(){
		try{
			ArrayList<String> tabBase = new ArrayList<String> (driver.getWindowHandles());
			int intotalhandles = tabBase.size();
			for(int i=intotalhandles;i!=1;i--){
				driver.switchTo().window(tabBase.get(intotalhandles-1));
				driver.close();
			}
			driver.switchTo().window(tabBase.get(0));


		}catch(Exception e){
			Log.info("Could not switch back to base Window");

		}
	}

	public void switchToNewWindow(String title) throws InterruptedException{
		Thread.sleep(10000);
		String parentWindowHandle = driver.getWindowHandle();
		Log.info("Current window is "+parentWindowHandle);
		Set <String> allWindowhandles = driver.getWindowHandles();
		Log.info("Total windows ="+allWindowhandles.size());
		for(String eachHandle : allWindowhandles){
			if(BaseTestClass.getBrowser().equalsIgnoreCase("ie")){
				//				if(!eachHandle.equals(parentWindowHandle)){
				//					driver.switchTo().window(eachHandle);
				switchToLastOpenedWindow();
				Log.info("Window switched IE");
				//				}

			}else{
				if(driver.switchTo().window(eachHandle).getTitle().equals(title)){
					switchToNewWindow();
					Log.info("Windows switched");
					break;
				}
			}

		}
	}	

	public void handleHTTPAuthenticationLogin(String username,String password){
		if(BaseTestClass.getBrowser().equalsIgnoreCase("firefox")){
			robot.mouseMove(30, 40);
			robot.mousePress(InputEvent.BUTTON1_MASK);
			robot.mouseRelease(InputEvent.BUTTON1_MASK);
		}		
		setClipboardData(username);		
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.delay(1000);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);			
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);			
		setClipboardData(password);		
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.delay(1000);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);			
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyRelease(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);				
	}

    public void waitForElementDisplayed(By locator) throws InterruptedException {
        boolean isFound = false;
        for (int second = 0; second <= 31; second++) {
            if (second <= 30) {
                Thread.sleep(1000);
                try {
                    if (driver.findElement(locator).isDisplayed()) {
                        Log.info("Wait ended...element Displayed");
                        isFound = true;
                        break;
                    }
                    else
                        continue;
                } catch (Exception e) {
                    continue;
                }
            }
        }
    }
}