package com.lam.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class FileOperations {

    String destPath = System.getProperty("user.dir") + "\\src\\test\\resources\\data\\uploadNewFiles\\";

    public String copyFile(String fileName){
        try {
            Log.info("Creating copy of file " +fileName+" with new name");
            String[] actualFileName = fileName.split("\\.");
            String testFileName = actualFileName[0];
            String fileExtension = "." + actualFileName[1];
            String newFileName = testFileName + RandomStringUtils.randomNumeric(5) + fileExtension;
            String actualPath = System.getProperty("user.dir") + "\\src\\test\\resources\\data\\uploadFiles\\"
                    + testFileName
                    + fileExtension;
            Path from = Paths.get(actualPath);
            Path to = Paths.get(destPath + newFileName);
            Files.copy(from, to, REPLACE_EXISTING);
            return newFileName;
        } catch (Exception e) {
            Log.info("File not get copied");
            return fileName;
        }
    }

    public void delete() {
        try {
            Log.info("Deleting the copy of file created from " + destPath);
            FileUtils.cleanDirectory(new File(destPath));
        } catch (IOException e) {
            Log.info("Not able delete the copy of file from" + destPath);
        }
    }
}
