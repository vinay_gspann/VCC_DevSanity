package com.lam.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


public class ScreenShots {

	public static void captureScreenshots(WebDriver driver){		  
		DateFormat format=new SimpleDateFormat("dd-MM-yyyy HH_mm_ss");
		Date d=new Date();
		String date=format.format(d);		  
		File srcFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(srcFile,new File(System.getProperty("user.dir")+"//src//test//resources//screenshots//VCC_Test_Fail_Screenshot"+date+".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
