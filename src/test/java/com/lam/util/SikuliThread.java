package com.lam.util;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class SikuliThread implements Runnable{
	private String userName, password;
	private Screen screen;
	
	public SikuliThread(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public void run()  {
		screen = new Screen();
		try {
			screen.wait("src/test/resources/sikuliImages/usernameChrome.png",40);
		} catch (FindFailed e1) {
			System.out.println("wait exception");
			e1.printStackTrace();
		}
		screen.type("src/test/resources/sikuliImages/usernameChrome.png", userName);
		screen.type("src/test/resources/sikuliImages/passwordChrome.png", password);
		try {
			screen.click(new Pattern("src/test/resources/sikuliImages/loginChrome.png"));
		} catch (FindFailed e) {
			e.printStackTrace();
		}
	}

}
